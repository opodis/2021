import Head from 'next/head'
import Link from 'next/link'
import { useEffect } from 'react'

import Layout from '../components/layout'
import Markdown from '../components/markdown'

import committee from '../../committee.yml';
import info from '../../info.yml';


function shuffle(containerId, elClass) {
  var container = document.getElementById(containerId);
  var elementsArray = Array.prototype.slice.call(container.getElementsByClassName(elClass));
    elementsArray.forEach(function(element){
    container.removeChild(element);
  })
  shuffleArray(elementsArray);
  elementsArray.forEach(function(element){
    container.appendChild(element);
  })
}

function shuffleArray(array, copy) {
    if(copy) array = array.slice();
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

const nameAffReg = /^(?<name>[^;]+);(?<affiliation>.*)|(?<onlyname>[^;]+)$/;

const NameAffl = ({name}) => {
  try {
    let chair = false;
    if(typeof name === 'object') {
      name = name.chair
      chair = true;
    }
    let m = name.match(nameAffReg).groups || {};
    return m.name
      ? <>{m.name}, <i>{m.affiliation}</i>{chair && <b> — chair</b>}</> 
      : <>{m.onlyname}</>
  } 
  catch(e)
  {
    return 'Unable to parse name '+name;
  }
}


export default function Home() {

  useEffect(() => {  
    shuffle('keynote-container', 'keynote-speaker');
  });
  return (
      <Layout>
        <section id="information">
          <div className="row">
            <h2>About OPODIS</h2>
          </div>
          <div className="row">
            <div className="column">
              <div>
                <Markdown markdown={info.about_opodis} />
              </div>
            </div>
          </div>
        </section>

        <section id="comitee">
          <div className="row">
            <h2>Comitee</h2>
          </div>
          <div className="row">
            <div className="column column">
                <b>General Chair</b><br/>
                  <NameAffl name={committee.general_chair} /><br/>
                <b>Program Chairs</b><br/>
                  {committee.program_chairs.map((c,i) => 
                  <div key={i}><NameAffl name={c} /></div>
                  )}
                <b>Steering Comitee</b><br/>
                  {committee.steering_committee.map((c,i) => 
                  <div key={i}><NameAffl name={c} /></div>
                  )}
                
                <b>Organizers</b><br/>
                  {committee.organizers.map((c,i) => 
                  <div key={i}><NameAffl name={c} /></div>
                  )}
                

            </div>
            <div className="column column">
                <b>Program Comitee</b><br/>
                {committee.program_committee.length
                ? committee.program_committee.map((c,i) => 
                    <div key={i}><NameAffl name={c} /></div>
                  )
                : 'TBD'}
            </div>
          </div>
        </section>


        <section id="keynotes">
          <div className="row">
            <h2>Keynote Speakers</h2>
          </div>
          <div className="row" id="keynote-container">
              {info.keynotes.length
                ? info.keynotes.map(({
                  name,
                  photo,
                  webpage,
                  title,
                  abstract
                },i) => 
                    <div className="column keynote-speaker" key={i}>
                      {photo && <div className="photo"><img src={photo} alt={"Picture of "+name} /></div>}
                      <NameAffl name={name} /><br/>
                      {webpage && <><a href={webpage} target="_blank">webpage</a><br/></>}
                      <strong>Title: </strong><span className="keynote-title">{title}</span><br/>
                      <strong>Abstract: </strong><span className="keynote-title">{abstract}</span>
                    </div>
                  )
                : 'TBD'}
          </div>
        </section>

        <section id="venue">
          <div className="row">
            <h2>Venue</h2>
          </div>
          <div className="row">
            <div className="column">
              <div>
                  <b>Where</b><br/>
                  <a href={info.where_url} target="_blank">{info.where}</a>
              </div>
            </div>
            <div className="column">
              <div>
                  <b>When</b><br/>
                  {info.when}
              </div>
            </div>
          </div>
          {!!info.map.iframe_url
            && <div>
            <iframe width="100%" height="300px" frameBorder="0" allowFullScreen src={info.map.iframe_url}></iframe>
            <p><a href={info.map.link_url}>See fullscreen</a></p>
            </div>
          }
        </section>

        <section id="sponsors">
          <div className="row">
            <h2>Sponsors</h2>
          </div>
          <div className="row">
              {info.sponsors.length
                ? info.sponsors.map((sponsor,i) => <div className={"column column-"+(sponsor.size || 25)} key={i}>
                    <div>
                      <a 
                      href={sponsor.url} 
                      target="_blank"
                      style={{
                        display: 'block',
                        textAlign: 'center'
                      }}>
                        <b>{sponsor.name}</b><br/>
                        <img src={sponsor.image} />
                      </a>
                    </div>
                </div>
                  )
                : 'TBD'}
          </div>
        </section>
    </Layout>
  )
}
