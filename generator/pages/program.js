import fs from 'fs'
import { join } from 'path'

import Head from 'next/head'
import Link from 'next/link'
import Markdown from '../components/markdown'
import { useEffect, useState } from 'react'

let sessionCounter = 0;
let lastSessionTime = '';


const authorToString = ({first, last}) => first+' '+last;

const Authors = ({authors}) => <span className="paper-authors">{
    authors.length > 1 
    ? authors
        .slice(0,authors.length-1)
        .map(authorToString).join(', ') + 
        ' and ' + 
        authorToString(authors[authors.length-1])
    : authorToString(authors[0])
}
</span>

const Details = ({paper, show}) => <div style={{display: show?'block':'none'}} className="details" id={`details-${paper.id}`}>
<div className="authors">
  {paper.authors.map((a,i) => <div key={i} className="author">
    <span className="author-name">
      {a.first} {a.last},
    </span>
    <span className="author-affiliation">
      {a.affiliation}
    </span>
  </div>
  )}
  </div>
  <div className="abstract"><strong>Abstract:</strong>
  {paper.abstract}
</div>
</div>


const Paper = ({paper, time, speakersAreVisible, day, userTimezone}) => {

  const [detailsVisible, showDetails] = useState(false);

return <div className="paper">
    <div className="time">{convertTime(day, userTimezone, time)}</div>
    <span className="speaker-name">{paper.authors.map(a => a.first+' '+a.last).join(', ')}</span>
    <span className="paper-title">{paper.title}</span>
    {speakersAreVisible && paper.speaker && <span key='paper-speaker' className="paper-speaker">Speaker: {paper.speaker} (onsite? {paper.onsite}{paper.video ? ". VIDEO": ''})</span>}
    <a onClick={() => showDetails(!detailsVisible)} className="open-details" id={`open-details-${paper.id}`}>+</a>
    <Details show={detailsVisible} paper={paper} />
  </div>
}


const timeAdd = ([hour, minute], inter) => {
  minute += inter;
  while(minute >= 60) {
    minute -= 60;
    hour += 1;
  }
  return hour + ':' + (minute<10?'0':'') + minute;
}

const convertTime = (day, timeZone, hourMinutes) => {
  try {
    let [hour, minutes] = hourMinutes.split(':');
    let date = new Date(Date.UTC(2020, 12, day, hour-1, minutes, 0));
                
    let intlDateObj = new Intl.DateTimeFormat('en-US', {
      timeZone,
      hour: "2-digit",
      minute: "2-digit",
      hour12: false
    });

    return intlDateObj.format(date);
  }
  catch(e) {
    return hourMinutes;
  }
}

const Keynote = ({keynote, userTimezone, day}) => { 

  const [detailsVisible, showDetails] = useState(false);

  return <div className="keynote">
    <div className="datetime">
      <span className="start">{convertTime(day, userTimezone, keynote.start)}</span>
      <span className="end">{convertTime(day, userTimezone, keynote.end)}</span>
    </div>
    <div className="keynote-image" style={{height:keynote.image_height || 'none'}}>
      <img src={keynote.image} />
    </div>
    <div className="info">
      <span className="talk-type">Keynote by </span>
      <span className="speaker-name">{keynote.keynote}</span> 
      <span className="speaker-affiliation">{keynote.affiliation}</span>
      <span className="title">{keynote.title}</span>
      {!detailsVisible && <a onClick={() => showDetails(true)}>show abstract</a>}
    </div>
    
    {detailsVisible && <div>
      {keynote.abstract}  
    </div>}
  </div>
}

const Break = ({session, day, userTimezone}) => <><div key="break-container" className="coffee-break">
  <span>{session.break}</span>
</div>
<span key="break-time" className="break-time">{convertTime(day, userTimezone, session.start || lastSessionTime)}</span>
</>

const Session = ({session, papers, speakersAreVisible, userTimezone, day}) => {
  if(session.end) lastSessionTime = session.end;

  if(session.keynote) return <Keynote day={day} userTimezone={userTimezone} keynote={session} />
  if(session.break) return <Break day={day} userTimezone={userTimezone} session={session} />

  let [shour, sminute] = session.start.split(':').map(i => parseInt(i));
  let st = shour*60+sminute;
  let [ehour, eminute] = session.end.split(':').map(i => parseInt(i));
  let et = ehour*60+eminute;

  let inter = et - st;
  inter /= session.papers.length;

  sessionCounter++;
  
  return  <div className="session">
    <div className="datetime">
      <span className="start">{convertTime(day, userTimezone, session.start)}</span>
      <span className="end">{convertTime(day, userTimezone, session.end)}</span>
    </div>
    <div className="info">
      <span className="session-nb">Session {sessionCounter}</span>
      {' - '}<span className="chair">chair: {session.chair}</span>
      <span className="session-name">{session.name}</span>
    </div>
    <div className="session-papers">
      {session.papers.map((p, i) => <Paper 
        day={day}
        userTimezone={userTimezone}
        speakersAreVisible={speakersAreVisible} 
        paper={Object.assign({}, papers[p], {id:p})} 
        time={timeAdd([shour, sminute], i*inter)}  
        key={`paper-${p}`} />)}
    </div>
  </div>
}



export default function Home({papers, program,speakers}) {


  useEffect(() => {   

    MathJax.Hub.Config({
      tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
    });


  })
  sessionCounter = 0;

  speakers.forEach(({title, speaker, onsite, video=false}) => {
    papers.forEach((p) => {
      if(p.title === title) {
        p.speaker = speaker;
        p.onsite = onsite;
        p.video = video;
      }
    })
  })

  const [visibleDay, showDay] = useState(-1);

  const [mapIsVissible, showMap] = useState(false);


  const [speakersAreVisible, showSpeakers] = useState(false);
  const [userTimezone, setUserTimezone] = useState(null);
  const [visibleUserTimezone, setVisibleUserTimezone] = useState(false);
  const defaultTimezone = "Europe/Paris";
  

  useEffect(function mount() {
    if(window.location.search === "?with-speakers")
    {
      showSpeakers(true);
    }
    
    setUserTimezone(Intl.DateTimeFormat().resolvedOptions().timeZone);
  });

  return (
    <div className="container">
      <Head>
        <title>OPODIS 2021 - Program</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" />
        <script type="text/javascript" async
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-MML-AM_CHTML">
        </script>
      </Head>

      <main>
        <br/>
        <h1 className="title"><Link href="/">OPODIS 2021</Link> - Program
        </h1>
        {mapIsVissible && <>
          <button key="button-map" onClick={() => showMap(false)}>Hide Map</button>
          <iframe key="ifram-map"  width="100%" height="300px" frameborder="0" allowfullscreen src="//umap.openstreetmap.fr/fr/map/opodis2021-venue_664047#15/48.5837/7.7503?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe><p><a href="//umap.openstreetmap.fr/fr/map/opodis2021-venue_664047#15/48.5787/7.7450">See in full screen</a></p>
        </>
        }
        {
          !mapIsVissible && <button onClick={() => showMap(true)}>Show Map</button>
        }

        <br/><br/>
        All times are in Central European Time. {
          userTimezone 
          && !visibleUserTimezone
          && userTimezone !== defaultTimezone 
          && <button onClick={() => setVisibleUserTimezone(true)} key='timezone-button'>Change timezone to {userTimezone}</button>
        }
        {
          visibleUserTimezone 
          && <button onClick={() => setVisibleUserTimezone(false)} key='timezone-button'>Change timezone back to {defaultTimezone}</button>
        }
        <br/>
        <a target="_blank" href="https://www.google.fr/maps/place/Esplanade,+67000+Strasbourg/@48.5820203,7.7656129,19z/data=!3m1!4b1!4m5!3m4!1s0x4796c8fc1977a657:0x5d9f3c831c6b73a1!8m2!3d48.5820203!4d7.7661614" >Google map link to lunches</a><br/>
        <a target="_blank" href="https://www.google.fr/maps/place/Ciarus/@48.5894773,7.7469949,19.03z/data=!4m8!3m7!1s0x4796c8455f904821:0x57fa95064bb134dc!5m2!4m1!1i2!8m2!3d48.589371!4d7.747445">Google map link to monday's diner</a><br/>
        <strong><a target="_blank" href="https://www.google.fr/maps/place/Embarcad%C3%A8re+BATORAMA+Cath%C3%A9drale/@48.5806676,7.7517642,18.74z/data=!4m5!3m4!1s0x4796c853671b9b15:0x13e5532f5874f1f1!8m2!3d48.5802267!4d7.7520883">Google map link to the Boat departure (The boat leave at 16:30 so arrive before)</a></strong><br/>
        <a target="_blank" href="https://www.google.fr/maps/place/Maison+Kammerzell/@48.5819424,7.7476212,17z/data=!3m2!4b1!5s0x4796c852582c1703:0x309eba4fc140566e!4m5!3m4!1s0x4796c99d65099471:0x1d08e489259a7c71!8m2!3d48.5819424!4d7.7498152" > Google map link to the Gala</a><br/>
        <br/>
        <div className="day-selector">
        <div onClick={() => showDay(-1)} className={visibleDay === -1 ? "active" : ""} id="gotoall">All</div>
        <div onClick={() => showDay(0)} className={visibleDay === 0 ? "active" : ""} id="goto14">December 13</div>
        <div onClick={() => showDay(1)} className={visibleDay === 1 ? "active" : ""} id="goto15">December 14</div>
        <div onClick={() => showDay(2)} className={visibleDay === 2 ? "active" : ""} id="goto16">December 15</div>
        </div>

        {program.map((day, day_i) => <div 
          style={{display: visibleDay === -1 || day_i === visibleDay ? 'block' : 'none'}}
          className="program-day"
          id={"day-"+(day_i+1)} 
          key={"day-"+(day_i+1)}>
            <div className="day-header">December {13+day_i}</div>
            {day.map((session, i) => <Session day={13+day_i} userTimezone={visibleUserTimezone ? userTimezone : defaultTimezone} speakersAreVisible={speakersAreVisible} papers={papers} session={session} key={"session-"+i} />
          )}
        </div>
        )}
      </main>
    </div>
  )
}

const rootDirectory = join(process.cwd(), '../')
export async function getStaticProps(context) {
  const fullPath = join(rootDirectory, 'accepted-papers.json');
  const fileContents = fs.readFileSync(fullPath, 'utf8')

  const fullPath_pro = join(rootDirectory, 'program.json');
  const fileContents_pro = fs.readFileSync(fullPath_pro, 'utf8')

  const speakers_pro = join(rootDirectory, 'paper-speaker.json');
  const speakers = fs.readFileSync(speakers_pro, 'utf8')
  return {
    props: {
      papers: JSON.parse(fileContents),
      program: JSON.parse(fileContents_pro),
      speakers: JSON.parse(speakers)
    }
  }
}
