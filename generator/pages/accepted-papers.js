import fs from 'fs'
import { join } from 'path'

import Head from 'next/head'
import Link from 'next/link'
import Markdown from '../components/markdown'

const authorToString = ({first, last}) => first+' '+last;

const Authors = ({authors}) => <span className="paper-authors">{
    authors.length > 1 
    ? authors
        .slice(0,authors.length-1)
        .map(authorToString).join(', ') + 
        ' and ' + 
        authorToString(authors[authors.length-1])
    : authorToString(authors[0])
}
</span>

export default function Home({papers}) {
  return (
    <div className="container">
      <Head>
        <title>OPODIS 2021 - Accepted Papers</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" />
      </Head>

      <main>
        <br/>
        <h1 className="title"><Link href="/">OPODIS 2021</Link> - Accepted Papers
        </h1>
        
        <section id="accepted-papers">
            <ul>
                {papers.map((paper,i) => <li key={i}>
                    <Authors {...paper} />
                    <span className="paper-title">{paper.title}</span>
                </li>)
                }
            </ul>
        </section>
      </main>
    </div>
  )
}

const rootDirectory = join(process.cwd(), '../')
export async function getStaticProps(context) {
  const fullPath = join(rootDirectory, 'accepted-papers.json');
  const fileContents = fs.readFileSync(fullPath, 'utf8')

  return {
    props: {
      papers: JSON.parse(fileContents)
    }
  }
}
