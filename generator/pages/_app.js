

import '../node_modules/milligram/dist/milligram.css'
import './global.css'
import './program.css';
import '../components/christmasLights.scss';

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}
