
import React, { useState } from 'react';
import { saveAs } from 'file-saver';

export default function Home() {

    const [data, setData] = useState([]);

    return (
    <div className="container">
        <div className="row">

Load Progression: <input onInput={(event) => {
        var file = event.target.files[0];
        if (!file) {
            return;
        }
        var reader = new FileReader();
        reader.onload = function(e) {
          var contents = e.target.result;
          try{
            let d = JSON.parse(contents);
            d = d.map(paper => ({
                title: paper.title,
                abstract: paper.abstract,
                authors: paper.authors.map(aut => ({
                    first: aut.first,
                    last: aut.last,
                    affiliation: aut.affiliation
                }))
            }))
            setData(d);
          }
          catch(e)
          {
            // ignore error
            // let the user retry
            console.log(e)
            alert('there was an error, please retry! (see the console log)');
          }
        }
        reader.readAsText(file);
    }
  } type="file" id="file-input" />


        </div>
        <div className="row">
            <pre style={{height:'200px', overflow:'scroll'}}>
                {data && JSON.stringify(data,null,2)}
            </pre>
        </div>
        <div className="row">
            <button disabled={data.length == 0} onClick={() => {

var blob = new Blob([JSON.stringify(data,null,2)], {type: "text/json;charset=utf-8"});
  saveAs(blob, "accepted-papers.json");


            }}>Save as</button>
        </div>
    </div>
    )
}
