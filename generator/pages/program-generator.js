import { useEffect } from "react"

import { saveAs } from 'file-saver';
import fs from 'fs'
import { join } from 'path'

export default ({papers}) => {

  useEffect(() => {


    let s = document.getElementById('sortable')

    window.sortableEl = [] 
    window.sortableEl.push(Sortable.create(sortable, {
      group: "sorting",
      sort: true,
      animation: 100
    }));
    
    document.getElementById("add-session")
    .addEventListener('click', () => {
      
      var node = document.createElement("div"); 
      document.getElementById("program-generator").appendChild(node); 
      node.innerHTML = '<h4>Session <input class="session-input"/></h4>';
      var sortableNode = document.createElement("div");
      node.appendChild(sortableNode); 
    
      window.sortableEl.push(Sortable.create(sortableNode, {
        group: "sorting",
        sort: true,
        animation: 100
      }));
      
    })

  })

return <div>
  <script src="https://raw.githack.com/SortableJS/Sortable/master/Sortable.js"></script>
  <div id="program-generator">
      <div>
        <h4>Session <input className="session-input" /></h4>
        <div id="sortable" className="list-group">
          {papers.map((p, i) => <div key={i} data-id={i} className="list-group-item">{p.title}</div>)}
        </div>
      </div>
      
    </div>
    <button id="add-session">Add Session</button><br/>
    <button onClick={() => {
      let sessions = []
      document.querySelectorAll('.session-input').forEach((el, i) => {
        sessions.push({
          name: el.value,
          papers: window.sortableEl[i].toArray()
        })
      })
      console.log(sessions);
      var blob = new Blob([JSON.stringify(sessions,null,2)], {type: "text/json;charset=utf-8"});
      saveAs(blob, "program.json");
    }}>Save to file</button>
</div>
}



const rootDirectory = join(process.cwd(), '../')
export async function getStaticProps(context) {
  const fullPath = join(rootDirectory, 'accepted-papers.json');
  const fileContents = fs.readFileSync(fullPath, 'utf8')

  return {
    props: {
      papers: JSON.parse(fileContents)
    }
  }
}
