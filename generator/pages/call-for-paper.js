import fs from 'fs'
import { join } from 'path'

import Head from 'next/head'
import Link from 'next/link'
import Markdown from '../components/markdown'

export default function Home({post}) {
  return (
    <div className="container">
      <Head>
        <title>OPODIS 2021 - Call for paper</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" />
      </Head>

      <main>
        <br/>
        <h1 className="title"><Link href="/">OPODIS 2021 - Home page</Link>
        </h1>
        <Markdown markdown={post} />
      </main>
    </div>
  )
}

const rootDirectory = join(process.cwd(), '../')
export async function getStaticProps(context) {
  const fullPath = join(rootDirectory, 'cfp.md');
  const fileContents = fs.readFileSync(fullPath, 'utf8')

  return {
    props: {
      post: fileContents
    }
  }
}
