import Head from 'next/head'
import Link from 'next/link'
import Markdown from '../components/markdown'

export default function Home({papers}) {
  return (
    <div className="container">
      <Head>
        <title>OPODIS 2021 - Registration</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" />
      </Head>

      <main>
        <br/>
        <h1 className="title"><Link href="/">OPODIS 2021</Link> - Registration
        </h1>
        
            Registration will open November 15th.<br/><br/>

            <a href="https://www.azur-colloque.fr/DR10/inscription/inscription/64/fr" target="_blank">⎋ Paid registration platform</a><br/>
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSeFL6lhZ16_4oVuCPB6cp1Q8SOwATApt1vhZHjF_21qhYK0Ag/viewform?usp=sf_link" target="_blank">⎋ Free registration form</a><br/>

            <table>
              <tr><td></td><th>Before November 28th</th><th>After November 28th <small>(before December 5th)</small></th></tr>
              <tr>
                <td>
                  <b>Full Registration</b><br/>
                  At least one author per accepted paper must <br/>
                  pay a full registration, even if the paper is presented online.
                </td>
                <td>450€</td><td>600€</td></tr>
              <tr><td><b>Student Registration</b></td><td>300€</td><td>600€</td></tr>
              <tr><td><b>Online participants</b></td><td>free</td><td>free</td></tr>
            </table>
      </main>
    </div>
  )
}
