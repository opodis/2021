import unified from 'unified';
import parse from 'remark-parse';
import remark2react from 'remark-react';
import Link from 'next/link';


function CustomLink({ children, href }) {
    return href.startsWith('/') || href === '' ? (
      <Link href={href}>
        <a>
          {children}
        </a>
      </Link>
    ) : (
      <a
        href={href}
        target="_blank"
        rel="noopener noreferrer"
      >
        {children}
      </a>
    );
  }

export default function Markdown({ markdown }) {
  const content = unified()
    .use(parse, {commonmark: true})
    .use(remark2react, {
        remarkReactComponents: {
          a: CustomLink,
        },
      })
    .processSync(markdown).result;

  return (
    <div>
      {content}
    </div>
  );
}
