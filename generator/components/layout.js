import Head from 'next/head'
import Link from 'next/link'
import { useState } from 'react';
import info from '../../info.yml';
import Markdown from './markdown';
import ChristmasLights from './christmasLights';

export default function Layout({ children, home }) {
  const [menuIsOpen, setMenuOpen] = useState(false);

  return (
    <>
        <Head>
            <meta name="og:title" content={info.title} />
            <link rel="icon" href="/favicon.png" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" />
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet"></link>
            <script type="text/javascript" async
                src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-MML-AM_CHTML">
            </script>
        </Head>
        <header>
            <ChristmasLights />
            <div className="jumbo">
                <div className="jumbo-cover" 
                    aria-label="OPODIS" 
                    style={{
                        backgroundImage: "url('strasbourg.jpg')"
                    }}>
                </div>
                <div id="jumbo-overlay" className="show">
                </div>
                <div className="container">
                    <h1 className="title on-background">
                        {info.title} <small>— {info.when}</small>
                    </h1>
                    
                    <h4 className="sub-title on-background">{info.sub_title}</h4>
                
                    <div className="clearfix">
                        <div className="float-left important-dates">
                            <article>
                            <div>
                                <b>Important dates</b><br/>
                                Abstract: {info.dates.abstract}<br/>
                                Full paper: {info.dates.paper}<br/>
                                Notification: {info.dates.notification}<br/>
                                Camera ready: {info.dates.camera_ready}<br/>
                                Conference: {info.when}
                            </div>
                            </article>

                            {info.proceedings_url && <div className="proceedings-url">
                                <article>
                                <div>
                                    <b>Proceedings:</b><br/>
                                    <a target="_blank" href={info.proceedings_url}>Link to the proceedings</a>
                                </div>
                                </article>
                            </div>
                            }
                        </div>
                        {info.submission_instructions && <div className="float-left submission-instructions">
                            <article>
                            <div>
                                <b>Submission instructions</b><br/>
                                <Markdown markdown={info.submission_instructions} />
                            </div>
                            </article>
                        </div>
                        }
                        {info.last_info && <div className="float-left last-info">
                            <article>
                            <div>
                                <b>Last Info</b><br/>
                                <Markdown markdown={info.last_info} />
                            </div>
                            </article>
                        </div>
                        }
                    </div>
                </div>
            </div>
        </header>
        <nav className={"sticky main-nav"+(menuIsOpen?' open':'')}>
            <a 
                className="main-nav-toggle"
                onClick={() => setMenuOpen(!menuIsOpen)}
                ><span></span>
                <span></span>
                <span></span></a>
            <div className="container">
                <ul className={"navigation-list float-right"}>
                    <li className="navigation-item">
                        <Link href="/registration">
                            <a className="navigation-link">Registration</a>
                        </Link>
                    </li>
                    <li className="navigation-item">
                        <Link href="/program">
                            <a className="navigation-link">Program</a>
                        </Link>
                    </li>
                    <li className="navigation-item">
                        <Link href="/call-for-paper">
                            <a className="navigation-link">Call for paper</a>
                        </Link>
                    </li>
                    <li className="navigation-item">
                        <Link href="/accepted-papers">
                            <a className="navigation-link">Accepted Papers</a>
                        </Link>
                    </li>
                    <li className="navigation-item">
                        <a 
                        className="navigation-link" 
                        href="#comitee"
                        onClick={() => setMenuOpen(false)}
                        >Comitee</a>
                    </li>
                    <li className="navigation-item">
                        <a 
                        className="navigation-link" 
                        href="#keynotes"
                        onClick={() => setMenuOpen(false)}>Keynotes</a>
                    </li>
                    <li className="navigation-item">
                        <a 
                        className="navigation-link" 
                        href="#venue"
                        onClick={() => setMenuOpen(false)}>Venue</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div className="container">
            <main>{children}</main>
        </div>
        <footer>
            <div className="container">
                Contact: <a href={"mailto:"+info.contact_email}>{info.contact_email}</a>
            </div>
        </footer>
    </>
  )
}


