OPODIS 2021 Call for Papers
======================================================

* Conference on Principles of Distributed Systems (OPODIS 2021)
13, 14, 15 December 2021
* Strasbourg, France (Note: the conference may take place virtually due to the coronavirus pandemic)
* Website : [https://opodis2021.unistra.fr/](https://opodis2021.unistra.fr/)
* Submission link : [https://opodis2021.bramas.fr](https://opodis2021.bramas.fr)

Important dates:
======================================================

* Abstract registration: August 31, 2021
* Submission deadline: September 5, 2021
* Acceptance notification: October 29, 2021
* Final version due: November 14, 2021
* Conference: December 13-15, 2021

Scope
======================================================

OPODIS is an open forum for the exchange of state-of-the-art knowledge on distributed computing and distributed computer systems. OPODIS aims at having a balanced program that combines theory and practice of distributed systems. OPODIS solicits papers in all aspects of distributed systems, including theory, specification, design, system building, and performance.

Topics of interest include, but are not limited to:

* Biological distributed algorithms
* Blockchain technology and theory
* Cloud computing, edge computing and data centers
* Communication networks 
* Concurrent and distributed data structures
* Dependability, security and privacy of distributed systems
* Design and analysis of distributed algorithms
* Distributed event processing
* Distributed graph algorithms
* Distributed machine learning and big data analytics
* Distributed operating systems and middleware
* Distributed storage, file systems and distributed database systems
* Embedded and energy-efficient distributed systems
* Formal methods and programming languages applied to distributed systems
* Game-theory and economical aspects of distributed computing
* High-performance, cluster, and grid computing
* Impossibility results for distributed computing
* Internet of things and cyber-physical systems
* Mobile agents, robots and population protocols
* Self-stabilizing, self-organizing and autonomous systems
* Synchronization, concurrent algorithms, shared and transactional memory
* Social networks, peer-to-peer networks and recommendation systems
* Wireless, mobile, ad-hoc and sensor networks


Double-blind review
======================================================

We will use double-blind peer review. All submissions must be anonymous. We will use a somewhat relaxed implementation of double-blind peer review: you are free to disseminate your work through arXiv and other online repositories and give presentations on your work as usual. However, please make sure you do not mention your own name or affiliation in the submission, and please do not include obvious references that reveal your identity. A reviewer who has not previously seen the paper should be able to read it without accidentally learning the identity of the authors. Please feel free to ask the PC chairs if you have any questions about the double-blind policy of OPODIS 2021.


Submissions
======================================================

Papers are to be submitted electronically at the following Submission link [https://opodis2021.bramas.fr](https://opodis2021.bramas.fr)
Submissions must be in English in pdf format and they must be prepared using the LaTeX style templates for LIPIcs and choosing the A4 paper option. A submission must not exceed 16 pages, including the cover page, figures, tables and references. The cover page should include the title of the paper, a list of keywords, and an abstract of 1 to 2 paragraphs summarizing the contributions of the submission. The submission must contain a clear presentation of the merits of the paper, including discussion of its importance, prior work, and an outline of key technical ideas and methods used to achieve the main claims. All of the ideas necessary for an expert to fully verify the central claims in the submission, including experimental results, should be included either in the submission or in a clearly marked appendix. The appendix will be read at the discretion of the reviewers!
If desired, the appendix can be a copy of the full paper. A submission must report on original research that has not previously appeared in a journal or conference with published proceedings. It should not be concurrently submitted to such a journal or conference. Any overlap with a published or concurrently submitted paper must be clearly indicated. The Program Chairs reserve the right to reject submissions that are out of scope, of clearly inferior quality, or that violate the submission guidelines. Each of the remaining papers will undergo a thorough reviewing process.


Publication
======================================================

OPODIS has post-proceedings published by Leibniz International Proceedings in Informatics (LIPIcs) in gold open access mode. The proceedings become available online, free of charge, after the conference. Preliminary versions of the proceedings will be available to participants at the conference electronically. The camera-ready version of a paper must have the same format and be of the same length as its submitted version.


Committee
======================================================

### Program Committee Co-Chairs

* Vincent Gramoli, The University of Sydney and EPFL 
* Alessia Milani, LIS, Aix-Marseille Université, France

### Program Committee:

* Emmanuelle Anceaume, CNRS, IRISA
* Hagit Attiya, Technion
* Alkida Balliu, University of Freiburg
* Alysson Bessani, LASIGE, FCUL, Universidade de Lisboa
* Borzoo Bonakdarpour, Michigan State University
* Janna Burman, Université Paris-Saclay,CNRS
* Armando Castaneda, UNAM
* Giuseppe Antonio Di Luna, Sapienza, Università di Roma
* Alexey Gotsman, IMDEA Software Institute
* Eschar Hillel, Yahoo !
* Colette Johnen, University of Bordeaux, LaBRI
* Sayaka Kamei, Hiroshima University
* Alex Kogan, Oracle Labs
* Dariusz Kowalski, University of Liverpool
* Kostas Magoutis, University of Crete and FORTH-ICS
* Avery Miller, University of Manitoba
* Marina Papatriantafilou, Chalmers University
* Ami Paz, University of Vienna
* Yvonne-Anne Pignolet, DFINITY Foundation
* Etienne Rivière, Université catholique de Louvain
* Luis Rodrigues, Universidade de Lisboa
* Jared Saia, University of New Mexico
* Stefan Schmid, University of Vienna
* Gokarna Sharma, Kent State University
* Michael Spear, Lehigh University
* Jukka Suomela, Aalto University
* Nitin Vaidya, Georgetown University
* Gauthier Voron, EPFL


Awards
======================================================

Awards will be given to the best paper and the best student paper (i.e., primarily written by a student). Eligibility for the best student paper award should be clearly indicated on the cover page.
