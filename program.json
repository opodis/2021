[
  [
    {
      "start": "08:45",
      "break": "Registration"
    },
    {
      "start": "09:00",
      "break": "Opening"
    },
    {
      "title": "Distributed algorithms: a challenging playground for model checking",
      "start": "09:10",
      "end": "10:10",
      "keynote": "Nathalie Bertrand",
      "affiliation": "INRIA, Rennes",
      "abstract": "Distributed computing is increasingly spreading, in advanced technological applications as well as in our daily life. Failures in distributed algorithms can have important human and financial consequences, so that is is crucial to develop rigorous techniques to verify their correctness. Model checking is a model-based approach to formal verification, dating back the 80's. It has been successfully applied first to hardware, and later to software verification. Distributed computing raises new challenges for the model checking community, and calls for the development of new verification techniques and tools. In particular, the parameterized verification paradigm is nowadays blooming to help proving automatically the correctness of distributed algorithms.",
      "image": "https://opodis2021.unistra.fr/nathalie.png"
    },
    {
      "break": "Coffee Break (without coffee)"
    },
    {
      "name": "Byzantine fault-tolerance",
      "chair": "Michel Raynal",
      "start": "10:40",
      "end": "12:20",
      "papers": [
        "1",
        "15",
        "18",
        "0"
      ]
    },
    {
      "break": "Lunch"
    },
    {
      "name": "Robots and mobile networks",
      "chair": "Sébastien Tixeuil",
      "start": "14:00",
      "end": "15:40",
      "papers": [
        "26",
        "4",
        "19",
        "21"
      ]
    },
    {
      "break": "Coffee Break (without coffee)"
    },
    {
      "name": "Population protocols",
      "chair": "Pascal Felber",
      "start": "16:10",
      "end": "17:25",
      "papers": [
        "16",
        "20",
        "8"
      ]
    },
    {
      "break": "Business Meeting"
    },
    {
      "start":"20:30",
      "break": "Diner at the Ciarus (link to map at the top of the page)"
    }
  ]
  ,
  [
    {
      "title": "A fresh look at the design and implementation of communication paradigms",
      "start": "09:10",
      "end": "10:10",
      "keynote": "Robbert van Renesse",
      "affiliation": "Cornell University, Ithaca, NY, USA",
      "abstract": "Datacenter applications consist of many communicating components and evolve organically as requirements develop over time. In this talk I will present two projects that try to support such organic growth. The first project, Escher, recognizes that components of a distributed systems may themselves be distributed systems. Escher introduces a communication abstraction that hides the internals of a distributed component, and in particular how to communicate with it, from other components. Using Escher, a replicated server can invoke another replicated server without either server having to even know that the servers are replicated. The second project, Scalog, presents a datacenter scale totally ordered logging service. Logs are increasingly a central component in many datacenter applications, but log configurations can lead to significant hiccups in the performance of those applications. Scalog has seamless reconfiguration operations that allow it to scale up and down without any downtime.",
      "image": "https://www.cs.cornell.edu/home/rvr/rvr.jpg"
    },
    {
      "break": "Coffee Break (without coffee)"
    },
    {
      "name": "Graphs",
      "chair": "Mikaël Rabie",
      "start": "10:40",
      "end": "11:55",
      "papers": [
        "10",
        "3",
        "13"
      ]
    },
    {
      "break": "Lunch"
    },
    {
      "name": "Coloring and dynamic graphs",
      "chair": "Rotem Oshman",
      "start": "13:35",
      "end": "14:50",
      "papers": [
        "22",
        "11",
        "7"
      ]
    },
    {
      "break": "Coffee Break (without coffee)"
    },
    {
      "name": "Verification/security",
      "chair": "Antonella del Pozzo",
      "start": "15:20",
      "end": "16:35",
      "papers": [
        "23",
        "9",
        "6"
      ]
    },
    {
      "start": "16:30",
      "break": "Boat Tour"
    },
    {
      "start": "21:00",
      "break": "Gala at the Maison Kammerzell"
    }
  ]
  ,
  [
    {
      "title": "Accountable distributed computing",
      "start": "09:10",
      "end": "10:10",
      "keynote": "Petr Kuznetsov",
      "affiliation": "INFRES, Telecom Paris, Institut Polytechnique de Paris",
      "abstract": "There are two major ways to deal with failures in distributed computing: fault-tolerance and accountability. Fault-tolerance intends to anticipate failures by investing into replication and synchronization, so that the system’s correctness is not affected by faulty components. In contrast, accountability enables detecting failures a posteriori and raising undeniable evidences against faulty components. In this talk, we discuss how accountability can be achieved, both in generic and application-specific ways. We also discuss how fault detection can be combined with reconfiguration, opening an avenue towards \"self-healing\" systems that seamlessly replace faulty components with correct ones. ",
      "image": "https://opodis2021.unistra.fr/petr.jpg",
      "image_height": "90px"
    },
    {
      "break": "Coffee Break (without coffee)"
    },
    {
      "name": "Reconfiguration and leader election ",
      "chair": "Emmanuelle Anceaume",
      "start": "10:40",
      "end": "12:20",
      "papers": [
        "12",
        "5",
        "14",
        "27"
      ]
    },
    {
      "break": "Lunch"
    },
    {
      "name": "Shared objects and data structures",
      "chair": "Alessia Milani",
      "start": "13:45",
      "end": "15:25",
      "papers": [
        "24",
        "2",
        "25",
        "17"
      ]
    },
    {
      "break": "Closing & Coffee Break (without coffee)"
    }
  ]
]
